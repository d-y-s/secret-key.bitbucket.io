$('.js-bestsellers-slider').owlCarousel({
    items: 3,
    loop: true,
    nav: true,
    navText: ['', ''],
    dots: false,
    responsive: {
        320 : {
            items: 1
        },
        640 : {
            items: 2
        },
        960 : {
            items: 3,
            margin: 35
        }
    }
});