$('.js-navbar-clone').find('ul').clone().appendTo('.js-nav-add');

$('.js-btn').on('click', function () {
    $(this).toggleClass('btn-active');
    $('.js-nav-add').stop().slideToggle(); 
});

$(document).on('click', function(e) {
    if($(e.target).closest('.b-burger-menu').length){
        return;
    }
    $('.js-btn').removeClass('btn-active');
    $('.js-nav-add').slideUp(); 
});