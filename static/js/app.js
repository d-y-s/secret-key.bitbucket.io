"use strict";

/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */

/**
 * Возвращает функцию, которая не будет срабатывать, пока продолжает вызываться.
 * Она сработает только один раз через N миллисекунд после последнего вызова.
 * Если ей передан аргумент `immediate`, то она будет вызвана один раз сразу после
 * первого запуска.
 */
function debounce(func, wait, immediate) {
  var timeout = null,
      context = null,
      args = null,
      later = null,
      callNow = null;
  return function () {
    context = this;
    args = arguments;

    later = function later() {
      timeout = null;

      if (!immediate) {
        func.apply(context, args);
      }
    };

    callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) {
      func.apply(context, args);
    }
  };
} // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
// MIT license


;

(function () {
  var lastTime = 0,
      vendors = ['ms', 'moz', 'webkit', 'o'],
      x,
      currTime,
      timeToCall,
      id;

  for (x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function (callback) {
      currTime = new Date().getTime();
      timeToCall = Math.max(0, 16 - (currTime - lastTime));
      id = window.setTimeout(function () {
        callback(currTime + timeToCall);
      }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }

  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
      clearTimeout(id);
    };
  }
})();

;

(function () {
  // Test via a getter in the options object to see if the passive property is accessed
  var supportsPassiveOpts = null;

  try {
    supportsPassiveOpts = Object.defineProperty({}, 'passive', {
      get: function get() {
        window.supportsPassive = true;
      }
    });
    window.addEventListener('est', null, supportsPassiveOpts);
  } catch (e) {} // Use our detect's results. passive applied if supported, capture will be false either way.
  //elem.addEventListener('touchstart', fn, supportsPassive ? { passive: true } : false);

})();

function getSVGIconHTML(name, tag, attrs) {
  if (typeof name === 'undefined') {
    console.error('name is required');
    return false;
  }

  if (typeof tag === 'undefined') {
    tag = 'div';
  }

  var classes = 'svg-icon svg-icon--<%= name %>';
  var iconHTML = ['<<%= tag %> <%= classes %>>', '<svg class="svg-icon__link">', '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<%= name %>"></use>', '</svg>', '</<%= tag %>>'].join('').replace(/<%= classes %>/g, 'class="' + classes + '"').replace(/<%= tag %>/g, tag).replace(/<%= name %>/g, name);
  return iconHTML;
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };
  /**
   * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
   */


  $('.js-bestsellers-slider').owlCarousel({
    items: 3,
    loop: true,
    nav: true,
    navText: ['', ''],
    dots: false,
    responsive: {
      320: {
        items: 1
      },
      640: {
        items: 2
      },
      960: {
        items: 3,
        margin: 35
      }
    }
  });
  $('.js-navbar-clone').find('ul').clone().appendTo('.js-nav-add');
  $('.js-btn').on('click', function () {
    $(this).toggleClass('btn-active');
    $('.js-nav-add').stop().slideToggle();
  });
  $(document).on('click', function (e) {
    if ($(e.target).closest('.b-burger-menu').length) {
      return;
    }

    $('.js-btn').removeClass('btn-active');
    $('.js-nav-add').slideUp();
  });
  $('.b-form-popup__form').on('submit', function (event) {
    var $this = $(this);
    event.preventDefault();
    $this.closest('.b-form-popup__section').addClass('success');
    $this.closest('.b-form-popup').find('.b-form-popup__send-success').addClass('success');
    setTimeout(function () {
      $this.closest('.app').find('.remodal-overlay').css('display', 'none');
      $this.closest('.app').find('.remodal-wrapper').css('display', 'none');
      $('html').removeClass('remodal-is-locked');
      $('body').css('padding-right', 0);
    }, 3000);
    setTimeout(function () {
      $this.closest('.b-form-popup__section').removeClass('success');
      $this.closest('.b-form-popup').find('.b-form-popup__send-success').removeClass('success');
    }, 3000);
  });
  $('.phone_us').mask('+7 (000) 000-00-00');
  $('.js-nav-to-anchor a').on('click', function (e) {
    e.preventDefault();
    var anchor = $(this);
    $('.js-btn').removeClass('btn-active');
    $('.js-nav-add').slideUp();
    $('html, body').stop().animate({
      scrollTop: $(anchor.attr('href')).offset().top
    }, 800);
    return false;
  });
});